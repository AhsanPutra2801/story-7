$(document).ready(function () {
    $('.move-down').click(function (e) {
        var self = $(this),
            item = self.parents('div.item'),
            swapWith = item.next();
        item.before(swapWith.detach());
    });
    $('.move-up').click(function (e) {
        var self = $(this),
            item = self.parents('div.item'),
            swapWith = item.prev();
        item.after(swapWith.detach());
    });
});