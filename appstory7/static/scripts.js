$(document).ready(function(){
  $( function() {
    $( "#accordion" ).accordion();
  } );
});

$('.buttonDown').click(function(e) {
  var self = $(this),
  content = self.parents('content'),
  swapwith = content.next();
  content.before(swapwith.detach());
});
$('.buttonUp').click(function(e) {
  var self = $(this),
  content = self.parents('content'),
  swapwith = content.prev();
  content.after(swapwith.detach());
});