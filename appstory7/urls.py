from django.urls import path
from . import views

app_name = 'appstory7'

urlpatterns = [
    path('', views.index, name='index'),
    path('accordion/',views.accordion,name="accordion"),
]