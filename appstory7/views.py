from django.shortcuts import render

# Create your views here.

def index(request):
    return render(request,'index.html')

def accordion(request):
    return render(request,'accordion.html')